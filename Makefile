## magickal cflags from https://stackoverflow.com/questions/19416981/running-luajit-object-file-from-c

CFLAGS=-O2 -Wall -Wl,-E 
CFLAGS := $(CFLAGS) $(shell pkg-config --cflags luajit)

#LDFLAGS=-lm -ldl $(shell pkg-config --libs openssl) 
LDFLAGS := $(LDFLAGS) $(shell pkg-config --libs luajit)

CC ?=		gcc

INSTALL =	install


all:: fennel-binary


repl.o: repl.lua
	luajit -b $< $@

fennel.o: fennel.lua
	luajit -b $< $@


fennelview.o: fennelview.lua
	luajit -b $< $@

# Force it to use gcc not cc
main.o: main.c
	gcc  -c $(CFLAGS)   $<

fennel-binary: fennel.o repl.o fennelview.o main.o
	gcc -o fennel-binary   fennel.o fennelview.o repl.o  main.o $(LDFLAGS)

clean::
	rm -rf *.o
	rm -rf fennel-binary


install: fennel-binary
	sudo install -m 0755 fennel-binary /usr/local/bin/
