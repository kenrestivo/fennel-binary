#!/usr/bin/env lua


-- Shamelessly copy-pasted from Fennel/fennel

local fennel_dir = arg[0]:match("(.-)[^\\/]+$")
package.path = fennel_dir .. "?.lua;" .. package.path
local fennel = require('fennel')
local unpack = unpack or table.unpack


local options = {
   sourcemap = true
}

local function dosafe(filename, opts, args)
   local ok, val = xpcall(function()
	 return fennel.dofile(filename, opts, unpack(args))
			  end, fennel.traceback)
   if not ok then
      io.stderr:write(val .. "\n")
      os.exit(1)
   end
   return val
end


-- Try to load readline library
local function tryReadline(opts)
   local ok, readline = pcall(require, "readline")
   if ok then
      readline.set_options({
            keeplines = 1000
      })
      function opts.readChunk(parserState)
	 local prompt = parserState.stackSize > 0 and '.. ' or '>> '
	 local str = readline.readline(prompt)
	 if str then
	    return str .. "\n"
	 end
      end
   end
end


ppok, pp = pcall(require, "fennelview")
if ppok then
   options.pp = pp
end

local initFilename = (os.getenv("HOME") or "") .. "/.fennelrc"
local init = io.open(initFilename, "rb")

tryReadline(options)

if init then
   init:close()
   -- pass in options so fennerlrc can make changes to it
   dosafe(initFilename, options, options)
end


print("Fennel " .. fennel.version .. " - Welcome to Fennel!")
fennel.repl(options)
