// via https://en.blog.nic.cz/2015/08/12/embedding-luajit-in-30-minutes-or-so/

// also includes
/*
** LuaJIT frontend. Runs commands, scripts, read-eval-print (REPL) etc.
** Copyright (C) 2005-2017 Mike Pall. See Copyright Notice in luajit.h
**
** Major portions taken verbatim or adapted from the Lua interpreter.
** Copyright (C) 1994-2008 Lua.org, PUC-Rio. See Copyright Notice in lua.h
*/


#include <stdio.h>
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include <string.h>

/* Convenience stuff */
static void close_state(lua_State **L) { lua_close(*L); }
#define cleanup(x) __attribute__((cleanup(x)))
#define auto_lclose cleanup(close_state) 



// stolen shamelessly from luajit
static int getargs(lua_State *L, char **argv, int n)
{
	int narg;
	int i;
	int argc = 0;
	
	while (argv[argc]) argc++;  /* count total number of arguments */
	
	narg = argc - (n + 1);  /* number of arguments to the script */
	
	luaL_checkstack(L, narg + 3, "too many arguments to script");
	
	for (i = n+1; i < argc; i++)
		lua_pushstring(L, argv[i]);
	
	lua_createtable(L, narg, n + 1);
	
	for (i = 0; i < argc; i++) {
		lua_pushstring(L, argv[i]);
		lua_rawseti(L, -2, i - n);
	}
	
	return narg;
}


////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
	int ret = 0;
	int n = 0; // no args to "script" at this time

	/* Create VM state */
	auto_lclose lua_State *L = luaL_newstate();

	if (!L){
		return 1;
	}
	
	luaL_openlibs(L); /* Open standard libraries */

	int narg = getargs(L, argv, n);
	lua_setglobal(L, "arg");

	fprintf(stderr, "narg! %d\n", narg); // shut up compiler
	
	/// this is the moment of truth
	luaL_loadstring(L, "repl = require(\"repl\")");
	
	// Now run the lua code
	ret = lua_pcall(L, 0, 0, 0);

	if (ret != 0) {
		fprintf(stderr, "%s\n", lua_tostring(L, -1));
		return 1;
	}

	lua_settop(L, 0); /* wipe everything out */
	return 0;
}
