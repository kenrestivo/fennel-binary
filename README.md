# Fennel Binary

A self-contained binary of the Fennel language http://fennel-lang.org

## Warnings

This is an experiment

### Nasty hacks

- fennelview.lua is vendored in directly instead of building it from fennelview.fnl
- No support for args passing in to the "script"?
- narg!
- lua_loadstring feels like cheating
- More!


## Prerequisites

- build-essential
- luajit-dev
- libssl-dev


## Compiling

	$ make

## Size

58KB, but dynamic

Deps include

```
	/lib64/ld-linux-x86-64.so.2 (0x00007f3e24df1000)
	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f3e2498c000)
	libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f3e24804000)
	libgcc_s.so.1 => /lib/x86_64-linux-gnu/libgcc_s.so.1 (0x00007f3e247ea000)
	libluajit-5.1.so.2 => /lib/x86_64-linux-gnu/libluajit-5.1.so.2 (0x00007f3e24b4d000)
	libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f3e24809000)
	linux-vdso.so.1 (0x00007ffe30beb000)
```

Plus possibly other libs loaded by libdl.so at runtime


## Future?

- Static binary envy from using go
  - The tricky bit will be getting luasocket and others to build as statics



# License

MIT licence


2019 ken restivo <ken@restivo.org>, 1994-2012 lua.org PUC-Rio, 2005-2017 Mike Pall, 2018 Calvin Rose and contributors

See LICENSE file
